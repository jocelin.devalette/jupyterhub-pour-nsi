#!/bin/bash

# Sript de mise en place du noyau OCaml pour jupyter

USER='caml'
chmod 755 /home/$USER
cd /home/$USER
su $USER -c "opam init -y"
su $USER -c "eval \$(opam env --switch=default)"
su $USER -c "opam install -y  jupyter"
su $USER -c ".opam/default/bin/ocaml-jupyter-opam-genspec"

cp -r /home/adminjh/ocaml /usr/local/share/jupyter/kernels/
