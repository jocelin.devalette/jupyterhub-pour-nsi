#!/bin/bash

# Script de creation automatique de comptes
# entree : fichier CSV separe par ;
# login;passwd ou alors login;passwd;1 pour les profs


FICH=$1
ADMIN="adminjh"
echo $FICH
for i in `cat $FICH`
do
USER=`echo $i | awk -F";" '{ print $1 }'`
PASS=`echo $i | awk -F";" '{ print $2 }'`
PROF=`echo $i | awk -F";" '{ print $3 }'`
if [ ! -d "/home/$USER"]; then
/usr/sbin/useradd $USER -s /bin/bash
echo "$USER:$PASS" | /usr/sbin/chpasswd
mkdir /home/$USER
# En cas de mise à jour -R pour conserver les anciens fichiers
chown -R $USER /home/$USER

if [ -n "$PROF" ]; then
        # Deploiement de la conf nbgrader
        mkdir -p /home/$USER/.jupyter
        cat /home/$ADMIN/.jupyter/nbgrader_config.py |sed -e "s/$ADMIN/$USER/g" > /home/$USER/.jupyter/nbgrader_config.py
        mkdir /home/$USER/source
        cp /home/$ADMIN/source/header.ipynb /home/$USER/source
        cp -r /home/$ADMIN/exemples /home/$USER
        mv /home/$USER/exemples/nbgrader/* /home/$USER/source
        rm -r /home/$USER/exemples/nbgrader
	adduser $USER  profs 
	# deploiement espace commun prof
	ln -s /srv/nbgrader/exchange/commun/ /home/$USER/commun
        # Activation de nbgrader pour le prof $USER
        chmod -R 700 /home/$USER
        chown -R $USER /home/$USER
        su $USER -c "jupyter labextension enable --level=user nbgrader:formgrader"
        su $USER -c "jupyter labextension enable --level=user nbgrader:create-assignment"
	su $USER -c "mkdir /home/$USER/notebooks_perso"
	echo "Utilisateur prof : $USER" 
else 
	su $USER -c "jupyter labextension disable --level=user nbgrader:formgrader"
	su $USER -c "jupyter labextension disable --level=user nbgrader:create-assignment"
	su $USER -c "jupyter labextension disable --level=user nbgrader:course_list"
	su $USER -c "jupyter labextension disable --level=user @deathbeds/jupyterlab-deck"
	su $USER -c "jupyter labextension disable --level=user @deathbeds/jupyterlab-fonts"
	cp /home/$ADMIN/.jupyter/nbgrader_config1.py  /home/$USER/.jupyter
	mv /home/$USER/.jupyter/nbgrader_config1.py /home/$USER/.jupyter/nbgrader_config.py
	su $USER -c "mkdir /home/$USER/notebooks_perso"
	echo "Utilisateur élève : $USER"
fi

else
	echo "$USER existe déjà"
fi

done
