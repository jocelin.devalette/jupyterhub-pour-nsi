# Installation automatique de JupyterLab via docker

Ce travail se base sur celui effectué par O.Lecluse : https://github.com/wawachief/jupyterhubDocker.git

**JupyterHub** est un outil permettant d'ajouter une fonctionnalité multi-utilisateurs à Jupyter. _nbgrader_ s'intègre à cette configuration afin de rendre automatique la soumission et la récupération des travaux par les élèves. Il nécessite par contre l'utilisation dans le lycée d'un serveur hébergeant la solution. Disposer d'un tel outil permet d'éviter d'avoir à installer jupyter en local car un accès distant via le navigateur suffit.

**Docker** est un outil de virtualisation d'application léger et très performant. Il permet de bénéficier d'un environnement jupyterhub indépendant du reste du système.

## Caractéristiques :
- Jupyterlab 4.2
- Notebook 7.2
- nbgrader v0.9.2 configuré : https://github.com/jupyter/nbgrader 
- noyaux Python3.10, LUA, Ocaml (installation avec script), C
- Prise en charge de fichiers Latex
- Mardown Myst : https://mystmd.org/
- Un espace d'échange commun pour les professeurs 


## Installation

La procédure d'installation est la suivante :
1. Récupérez le matériel nécessaire à la fabrication de l'image Docker
```console
git clone https://gitlab.com/jocelin.devalette/jupyterhub-pour-nsi.git
```

2. Installez docker sur votre machine. Sous linux, tapez simplement
```console
apt-get install docker.io
```
docker.io est la version de docker packagée par debian/ubuntu. Si vous rencontrez des difficultés avec cette version, vous pouvez utiliser la version officielle docker-ce dont l'installation est décrite ici :

Vous trouverez un excellent tutoriel ici : https://aymeric-cucherousset.fr/installer-docker-debian-11/

Utiliser une ou l'autre de ces versions ne changera rien pour la suite.


3. Allez dans le dossier *jupyterhub-pour-nsi* et construisez votre image Docker

```console
cd jupyterhub-pour-nsi
docker build -t jhub .
```
N'oubliez pas le . à la fin de la seconde commande !

**Attention** : si vous avez déjà utiliser des imagesdocker pour jupyterhub, il est possible qu'elles soient encore en cache, donc pour vider le cache :
```console
docker builder prune
```
4. Lancez votre image
```console
docker run -i -p8000:8000 --name jhub jhub
```

Le serveur **jupyterhub** est à présent opérationnel. Ouvrez un navigateur et allez à l'adresse
http://127.0.0.1:8000 (ou http://_votre_adresse_ip:8000 via le réseau).

## Installer le noyau OCaml
1. Se connectez en *adminjh* sur le serveur (mot de passe _wawa_)

2. Lancer un terminal et se connecter en root, le mot de passe est le même que celui de *adminhjh*
```console
sudo su
```

3. Lancer le script d'installation du noyau OCaml qui se trouve dans le répertoire `home` de *adminjh*.
```console
./ocaml_install.sh
```

## Gérer la persistance des données
Si vous mettez en place un serveur en production, vous voudrez que vos données survivent même si vous effacez le container pour en reconstruire un propre à partir d'une image. Les **volumes** docker sont vos amis ! Grâce à eux, vous pourrez externaliser le stockage de certains dossiers hors du container. Pour cette installation de jupyterhub, je recommande deux volumes 
- un volume pour les espaces personnels de stockage (jh_home)
- un volume pour la zone d'échange nbgrader (jh_exchange)

Pour créer ces deux volumes, tapez les commandes suivantes
```console
docker volume create jh_home
docker volume create jh_exchange
```

Pour créer un container utilisant ces volumes, il faut juste ajouter le paramètre 
  
  -v NOM_VOLUME:ARBO_DANS_CONTAINER :

```console
docker run -it --name jhub -p 8000:8000 -v jh_home:/home -v jh_exchange:/srv/nbgrader/exchange jhub
```

et voilà, en modifiant juste la ligne de création du container, vos données sont persistantes ! Vous pouvez effacer le container et en recréer un, vous retrouverez vos données. Vous avez maintenant un serveur opérationnel pour la production.

# Utiliser nbgrader
## Les comptes par défaut

Par défaut pour que puissiez tester les outils il existe des comptes par défaut :

- Comptes prof : prof1,prof2,...,prof9 - mot passe *wawa*
- Comptes éléves : eleve1,eleve2,...,eleve 30 - mot de passe *toto*

Il existe un utilitaire `import_comptes.sh` qui permet d'importer les comptes à partir d'un fichier CSV. 
Le format est le suivant : `login;mdp;`
Les comptes profs ont en plus un `1` à la fin, le séparateur est le `;`.

```csv
curie;AzertY;1
ducobu;qwerty;
``` 
## Rédiger et distrbuer un Notebook - Prof

## Récupérer, rédiger et rendre un Notebook - Elève

## Ramasser et corriger les Notebooks - Prof

# Quelques astuces avec Docker
## Sauvegarde et restauration des données

Dans la commande ci-dessous, nous allons créer un nouveau container basé sur une ubuntu qui va accéder aux volumes de notre container nommé **jhub** et fabriquer une archive *tar* qui sera stockée dans le répertoire courant de la machine hôte. L'option **--rm** permet d'effacer ce container temporaire qui ne sert qu'à la récupération des données.
```console
docker run --rm --volumes-from jhub -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /home /srv/nbgrader/exchange
```
La ligne suivante va restaurer l'archive **backup.tar** réalisée ci-dessus d'un nouveau container **jhub_new** que l'on a déjà lancé.
```console
docker run --rm --volumes-from jhub_new -v $(pwd):/backup ubuntu bash -c "cd / && tar xvf /backup/backup.tar"
```
Ces deux méthodes montrent donc comment transférer le contenu d'un container à un autre. On peut ainsi migrer facilement une installation jupyterhub sur une autre machine.

## Quelques commandes docker utiles :
- Pour fermer l'image, tapez CTRL+C

- Pour réouvrir à nouveau ce container, 
```console
docker start -i jhub
```

- Pour connaître la liste des containers
```console
docker ps -a
```

- Pour connaître la liste des images
```console
docker images
```

- Pour effacer un container (afin de repartir de l'image propre, par exemple de début d'année)
**Attention** les données contenues dans le container seront **détruites** !!!
```console
docker rm CONTAINER_ID
```
- Pour effacer l'image construite (attention !) :
```console
docker rmi jhub
```

- pour lister les volumes :
docker volume ls

- pour avoir des informations sur un volume :
docker volume inspect my-vol
