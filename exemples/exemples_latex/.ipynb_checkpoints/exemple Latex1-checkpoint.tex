\usepackage[colorlinks]{hyperref}
\hypersetup{pdftitle={Introduction aux polynômes},
            pdfsubject={Présentation des polynômes en PTSI},
            pdfauthor={Jean-Michel Sarlat <jsarlat@planete.net>},
            pdfkeywords={polynômes,construction},
            pdfpagemode={FullScreen}
}
 
\def\prec{\Acrobatmenu{PrevPage}{\color{green}\Large$\blacktriangleleft$}}
\def\suiv{\Acrobatmenu{NextPage}{\color{green}\Large$\blacktriangleright$}}
\rightheader{\suiv}
\leftheader{\prec}
 
\title{Introduction aux polynômes}
\author{Jean-Michel Sarlat}
\date{8 février 2001}
 
%=====================================================================
% Ensembles fondamentaux
\def\R{\mathbf{R}}
\def\C{\mathbf{C}}
\def\K{\mathbf{K}}
\def\N{\mathbf{N}}
\def\Q{\mathbf{Q}}
\def\Z{\mathbf{Z}}
% Symboles, notations
\let\congru=\equiv
\def\build#1_#2^#3{\mathrel{\mathop{\kern 0 pt#1}\limits_{#2}^{#3}}}
\def\equiv#1{\build \sim_{#1}^{}}																												% symbole équivalent à en #1
\def\negli#1{\build \ll_{#1}^{}} 																												% symbole négligeable en #1
\def\Sup#1{\build\hbox{Sup}_{#1}^{}}																									% symbole Sup
\def\inf{\mathop{\rm Inf}\nolimits}																										% symbole Inf
\def\sup{\mathop{\rm Sup}\nolimits}																										% symbole Sup
\def\Inf#1{\build\hbox{Inf}_{#1}^{}}																									% symbole Inf
\def\max#1{\build\hbox{Max}_{#1}^{}}																									% symbole Max
\def\min#1{\build\hbox{Min}_{#1}^{}}																									% symbole Min
\def\egaldef{\build{=}_{}^{\hbox{\scriptsize def}}}
\def\egalipp{\build{=}_{}^{\hbox{\scriptsize IPP}}}	
\def\equivdef{\build{\iff}_{}^{\hbox{\scriptsize def}}}	
\def\abs#1{\vert #1 \vert}
\def\val{\mathop{\rm{val}}\nolimits}
\def\grad{\mathop{\vect{{\rm{grad}}}}\nolimits}
\let\le\leqslant
\let\ge\geqslant
\let\leq\leqslant
\let\geq\geqslant
 
\begin{document}
 
\maketitle
\definecolor{gris}{rgb}{0.9,0.9,0.9}
\definecolor{bleufonce}{rgb}{0,0,0.5}
 
\color{bleufonce}
 
%% ! La commande suivante n'est pas comprise par la package color
%% avec pdflatex
\pagecolor{gris}
 
 
\foilhead{Les nombres à la base}
 
\MyLogo{}
 
On désigne par $\K$ l'un des ensembles  $\R$ ou $\C$. 
$\K$ est muni de deux lois de compositions internes~: $+$ et $\times$;
la richesse de leurs  propriétés  lui confère une structure 
de \textbf{corps commutatif}~: 
\begin{itemize}
	\item  $(\K,+)$ est un \textbf{groupe commutatif}, ceci est la 
	conséquence du fait que
	\begin{itemize}
		\item  $+$ est associative.
		\item  $+$ est commutative.
		\item  $+$ admet un élément neutre~:  $0$.
		\item  Tout élément $x$ de $\K$ admet un symétrique~: $-x$.
	\end{itemize}
	\item  $(\K^*,\times)$ est un groupe commutatif (Rappel~: 
	$\K^*=\K\setminus\{0\}$ est l'ensemble des éléments de $\K$ qui ont 
	un inverse).
	\item  $\times$ est distributive à gauche et à droite par rapport 
	à $+$, c'est-à-dire~: 
	$$\forall (x,y,z)\in\K,\, x\times(y+z)=x\times y+x\times z\hbox{ et }
	(y+z)\times x =y\times x + z\times x$$
\end{itemize} 
 
\foilhead{Les nombres à la base (suite)}
 
\begin{enumerate}
	\item Dans la pratique et lorsqu'il n'y a pas ambiguïté, on omet le signe 
	$\times$. 
	\item   $\R$ et $\C$ ne sont pas les seuls ensembles ayant une 
	structure de corps (ie. qui vérifient les propriétés énumérées 
	ci-dessus), on vérifie sans peine que c'est aussi le cas pour $\Q$ 
	mais que cela est faux pour $\Z$. On rencontrera dans la suite 
	immédiate du cours sur les polynômes, un corps bien particulier qui 
	entretient avec l'ensemble des polynômes le même rapport que $\Q$ 
	avec $\Z$.
\end{enumerate}
 
\foilhead{Les suites nulles à partir d'un certain rang}
 
On note $(a_{0},a_{1},a_{2},\ldots,a_{n},0,\ldots)$ une suite 
(infinie) d'éléments de $\K$ nuls à partir d'un certain rang.  $n$ désigne un 
entier naturel tel que au delà du rang $n$ tous les termes sont nuls, 
ce qui ne signifie  d'ailleurs pas que les termes $a_{i}$ ($i=0\ldots n$) 
sont tous non nuls.\\[2mm]
Exemples~: $(0,1,-1,0,2,0,0,0,\ldots)$, $(\pi,e,\sqrt2,0,\ldots)$ 
etc...\\[2mm]
Dans le but d'alléger les écritures, une telle suite  sera notée 
$A=\left\{(a_{i})_{i\in\N},n\right\}$\footnote{où il faut comprendre 
que si $i\ge n$ alors $a_{i}=0$}.
On définit alors trois lois de composition dans l'ensemble de ces  suites.
 
\foilhead{Addition}
	$$A+B=\left\{(a_{i})_{i\in\N},n\right\}+\left\{(b_{i})_{i\in\N},m\right\}
	\egaldef\left\{(a_{i}+b_{i})_{i\in\N},\sup\{n,m\}\right\}$$
	La quantité $\sup\{n,m\}$ apparaît ci-dessus puisque si $i$ est 
	supérieur au plus grand des deux nombres $n$ et $m$ alors 
	$a_{i}+b_{i}=0$, la suite ainsi définie est bien nulle à partir d'un 
	certain rang.
\foilhead{Multiplication par un scalaire} 
	$$\lambda.A=\lambda. \left\{(a_{i})_{i\in\N},n\right\}\egaldef
	\left\{(\lambda a_{i})_{i\in\N},n\right\}$$
	$\lambda$ désigne ici un élément de $\K$, c'est un \textbf{scalaire} (c'est 
	l'autre mot pour désigner un nombre qui, comme ici, intervient dans 
	une multiplication externe).
\foilhead{Produit}
	$$A\times B=\left\{(a_{i})_{i\in\N},n\right\}\times\left\{(b_{i})_{i\in\N},m\right\}
	\egaldef\left\{\left((\sum_{k=0}^{k=i}a_{k}b_{i-k}\right)_{i\in\N},n+m\right\}$$
	Si on y regarde de près, dès que $i>n+m$ 
        la quantité $\sum_{k=0}^{k=i}a_{k}b_{i-k}$ est nulle puisque 
	tous ses termes sont nuls (l'un des facteurs est 
	\emph{nécessairement nul}).
 
\foilhead{Suites particulières}
 
Il y a deux suites particulières qui sont~:  
$$0\egaldef (0,0,0,0,0,\ldots)$$
$$1\egaldef (1,0,0,0,0,\ldots)$$
ce sont respectivement les éléments neutres pour l'addition et le 
produit.\\
On observe que l'application $\varphi:\lambda\longmapsto 
\lambda.1=(\lambda,0,\ldots)$ possède les propriétés suivantes~: 
$$\forall (\lambda,\mu)\in\K,\, 
\varphi(\lambda+\mu)=\varphi(\lambda)+\varphi(\mu)\hbox{ et 
}\varphi(\lambda\times \mu)=\varphi(\lambda)\times\varphi(\mu)$$
L'application $\varphi$ est injective, c'est un \textbf{morphisme} qui 
permet d'identifier le scalaire $\lambda$ et la suite $(\lambda, 
0,\ldots)$.
 
\foilhead{L'indéterminée}
 
Parmi les suites qui ne peuvent pas être identifiées à un scalaire, 
il en est une particulière que l'on note $X$, que l'on nomme 
\textbf{l'indéterminée} et qui est telle que~: 
$$X=(0,1,0,0,0,\ldots)$$
On vérifie alors~: 
$$X^2\egaldef X\times X =(0,0,1,0,0,\ldots)$$
$$X^3\egaldef X\times X^2 =(0,0,0,1,0,\ldots)$$
et ainsi de suite ...\\
Pour être complet, en hommage à une telle régularité, on pose 
$X^0\egaldef(1,0,0,\ldots)=1$.\\
Ainsi, la suite 
$A=(a_{0},a_{1},\ldots,a_{n},0,\ldots)=\left\{(a_{i})_{i\in\N},n\right\}$ 
peut s'écrire~: 
$$A=a_{0}+a_{1}X+a_{2}X^2+\ldots+a_{n}X^n=\sum_{i=0}^{i=n}a_{i}X^i$$
 
\foilhead{L'indéterminée (suite)}
 
Vous remarquerez que j'ai procédé à l'identification de 
$(a_{0},0,\ldots)=a_{0}.1$ avec $a_{0}$.
 
Et voilà donc les polynômes tels 
qu'on les connaît ? Ce n'est pas sûr, l'indéterminée ne représente pas un 
nombre, elle est même définie comme un objet qui, dans le contexte 
de cette étude, est tout sauf un scalaire~! En fait $X$ peut être 
considérée comme un objet extérieur à un ensemble (ici $\K$) qu'on lui adjoint de façon à 
constituer un ensemble plus vaste (une \textbf{extension}) où les 
lois de compositions connues à la base se généralisent.\\[1mm]
Puisque les scalaires et l'indéterminée suffisent pour écrire les 
suites nulles à partir d'un certain rang, on note $\K[X]$ 
leur ensemble\footnote{Peut-être certains d'entre-vous se demandaient-ils 
pourquoi on tardait à nommer cet ensemble, qu'ils soit récompensés 
d'avoir été patients !}, on les nomme \textbf{polynômes} et  on dit 
qu'ils sont à coefficients dans $\K$.
 
\foilhead{TEST}
 
Pour vérifier que ce qui précède vous éclaire sur des 
pratiques antérieures et pour en dégager l'aspect \textbf{formel}, donner 
un sens aux écritures~: 
$\R[i]$, $\Q[\sqrt2]$, $\C[x\mapsto x]$.
 
\foilhead{L'anneau des polynômes}
On montre que l'ensemble $\K[X]$ muni des deux lois de composition
internes~: $+$ et $\times$ possède une structure d'anneau\footnote{
En réalité quand on considère la troisième loi (externe), la 
structure de $\K[X]$ est plus riche, mais ceci correspond à un cours à
venir.}~:
\begin{itemize}
	\item  $(\K[X],+)$ est un groupe commutatif
	\item  $\times$ est associative et commutative.
	\item  $\times$ possède un élément neutre~: $1$
	\item  $\times$ est distributive par rapport à $+$.
\end{itemize}
$\K[X]$ n'est pas un corps~:  tous les polynômes non nuls n'ont pas 
nécessairement un inverse.
 
\foilhead{Les caractéristiques d'un polynôme}
 
La notation générale d'un polynôme est 
$\displaystyle P=\sum_{k=0}^{\infty}a_{k}X^k$. Les nombres $a_{k}$ sont les 
\textbf{coefficients} de $P$, ce sont des éléments de $\K$ et ils sont 
nuls à partir d'un certain rang. 
\begin{itemize}
	\item  Un polynôme est le polynôme nul ssi tous ses coefficients sont 
	nuls.
	\item  Deux polynômes sont égaux ssi ils ont les mêmes coefficients.
	\item  Le \textbf{degré} du polynôme $\displaystyle P = 
	\sum_{k=0}^{\infty}a_{k}X^k$ (noté $\deg P$ ) est, si le 
	polynôme est non nul, le plus grand indice $n$ tel que $a_{n}\ne 0$, 
	sinon il est égal à $-\infty$ (convention).
    \item La \textbf{valuation} du polynôme $\displaystyle 
    \sum_{k=0}^{\infty}a_{k}X^k$ (notée $\val P$) est, si le polynôme 
    est non nul, le plus  petit indice $m$ tel que $a_{m}\ne 0$, 
    sinon elle est égale à $+\infty$ (convention).
\end{itemize}
 
\foilhead{Les caractéristiques d'un polynôme (suite)}
 
\begin{itemize}
   \item Le \textbf{coefficient dominant} d'un polynôme non nul est le 
    coefficient dont l'indice est égal au degré du polynôme.
    \item Un polynôme est \textbf{unitaire} ou \textbf{normalisé} si, 
    et seulement si son coefficient dominant est égal à~$1$.
\end{itemize}
 
Le degré et la valuation vérifient les propriétés suivantes pour tous 
polynômes $P$ et~$Q$~: 
$$\deg(P+Q)\le \sup\{\deg P,\deg Q\}$$
$$\deg(P\times Q)=\deg P+\deg Q$$
$$\deg(\lambda.P)=\deg P\hbox{ si }\lambda\ne0$$
$$\val(P+Q)\ge \inf\{\val P,\val Q\}$$
$$\val(P\times Q)=\val P+\val Q$$
$$\val(\lambda.P)=\val P\hbox{ si }\lambda\ne0$$
 
\foilhead{Exemple} $P=-X^3+X^2+2X^5+X^4$. $P$ est non nul, son degré 
est $5$, sa valuation $2$ et son coefficient dominant est $2$.\\[2mm]
Pour éviter toute erreur de lecture on développe un polynôme suivant 
les puissances croissantes ou les puissances décroissantes.
 
 
\foilhead{La suite du cours}
 
Elle va se développer suivant le plan suivant~: 
\begin{enumerate}
	\item  Divisibilité
	\item  Fonction polynôme
	\item  Polynôme dérivé
	\item  Formule de Taylor
	\item  Zéros d'un polynôme
	\item  Polynômes scindés
	\item  Relations entre racines et coefficients
\end{enumerate}
 
\rightheader{}
 
\end{document}
 