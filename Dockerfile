FROM jupyterhub/jupyterhub

LABEL maintainer="Jocelin Devalette <jocelin.devalette@egd.mg>"

USER root

ARG JH_ADMIN=adminjh
ARG JH_PWD=wawa

RUN apt-get update && apt-get install -yq --no-install-recommends \
	python3-pip

RUN useradd $JH_ADMIN --create-home --shell /bin/bash
RUN pip install SQLALchemy
RUN pip install --upgrade pip
RUN pip install jupyter
RUN pip install --upgrade jupyterhub
RUN jupyterhub upgrade-db
# RUN pip install SQLAlchemy==1.4.49
# RUN pip install jupyter_server_terminals==0.4.0
RUN pip install nbgrader==0.9.2
RUN pip install jupyter_contrib_nbextensions

COPY nbgrader_config.py /home/$JH_ADMIN/.jupyter/nbgrader_config.py
COPY jupyterhub_config.py /srv/jupyterhub/

RUN echo "$JH_ADMIN:$JH_PWD" | chpasswd

RUN groupadd admin && \
	usermod -a -G admin $JH_ADMIN

RUN mkdir -p /home/$JH_ADMIN/.jupyter && mkdir /home/$JH_ADMIN/source
COPY nbgrader_config.py /home/$JH_ADMIN/.jupyter/nbgrader_config.py
COPY header.ipynb /home/$JH_ADMIN/source
RUN chown -R $JH_ADMIN /home/$JH_ADMIN && chmod 700 /home/$JH_ADMIN
COPY --chown=1000 exemples /home/$JH_ADMIN/exemples
COPY import_comptes.sh /home/$JH_ADMIN

COPY comptes.csv /root
COPY import_comptes.sh /usr/bin
COPY killJup.sh /usr/bin
COPY checkmem.sh /usr/bin
RUN chmod 755 /usr/bin/*.sh

RUN pip install jupyterlab-language-pack-fr-FR
# RUN jupyter labextension disable @jupyterlab/extensionmanager-extension
RUN apt-get install -yq graphviz

RUN pip install mobilechelonian \
    pandas \
    matplotlib  \
    folium  \
    geopy \
    pillow \
    networkx \
    graphviz \
    jupyterlab-pytutor \
    jupyterlab-spellchecker \
    jupyterlab-logout \
    jupyterlab-deck
 
RUN apt-get install -yq --no-install-recommends nano\
	git \
        g++ \
        gcc \
	sudo \
        netbase \ 
	make \
        xz-utils \
        zlib1g-dev \
        gnupg 
RUN jupyter labextension disable --level=sys_prefix nbgrader:course-list
RUN mkdir -p /srv/nbgrader/exchange && \
    chmod ugo+rw /srv/nbgrader/exchange
RUN apt-get install bc

COPY nbgrader_config1.py /home/$JH_ADMIN/.jupyter/nbgrader_config1.py
# Configuration nb grader
RUN jupyter labextension disable --level=sys_prefix  nbgrader:create_assignment
RUN useradd prof --create-home --shell /bin/bash
RUN groupadd profs
RUN usermod -G profs prof
RUN mkdir -p /srv/nbgrader/exchange/commun && \
    chown prof:profs /srv/nbgrader/exchange/commun &&\
    chmod 770 /srv/nbgrader/exchange/commun
RUN mkdir -p /srv/nbgrader/exchange/commun/.ipynb_checkpoints &&\
    chown prof:profs /srv/nbgrader/exchange/commun/.ipynb_checkpoints &&\
    chmod 770 /srv/nbgrader/exchange/commun/.ipynb_checkpoints
RUN /usr/bin/import_comptes.sh /root/comptes.csv

# Installation noyau Lua
RUN apt-get install lua5.4
RUN pip install ilua

# installation noyau OCaml
ARG CAML_ADMIN=caml
ARG CAML_PWD=zozo
RUN useradd $CAML_ADMIN --create-home --shell /bin/bash
RUN echo "$CAML_ADMIN:$CAML_PWD" | chpasswd
RUN mkdir /home/$JH_ADMIN/ocaml
COPY ocaml/* /home/$JH_ADMIN/ocaml/

RUN apt-get install -yq opam
RUN apt-get install -yq zlib1g-dev libffi-dev libgmp-dev libzmq5-dev
COPY ocaml_install.sh /home/$JH_ADMIN/
RUN chmod 755 /home/$JH_ADMIN/ocaml_install.sh
# Doit être lancé une fois le docker lancé.. Ne marche pas pendant le build ??
# RUN /home/$JH_ADMIN/ocaml_install.sh

# installation noyau C
RUN pip install jupyter-c-kernel
RUN install_c_kernel
RUN mkdir /usr/local/share/jupyter/kernels/c
COPY c/* /usr/local/share/jupyter/kernels/c/

# Pour rcwiz
RUN apt-get install -yq python3-dev graphviz-dev libgraphviz-dev pkg-config  
RUN pip install pygraphviz

# Pour export pdf/latex (lourd !)
RUN apt-get install -yq pandoc
RUN apt-get install -yq texlive-xetex texlive-fonts-recommended texlive-plain-generic 
# Mise à jour nbgrader
COPY clearsolutions.py /usr/local/lib/python3.10/dist-packages/nbgrader/preprocessors/clearsolutions.py
# Installation complémentaire
RUN pip install ipythonblocks \
	metakernel \
	ipycanvas \
	ipyturtlenext  

RUN apt-get update
RUN apt-get install -yq texlive-xetex
RUN apt-get install -yq pandoc
RUN pip install nbconvert
RUN pip install jupyterlab-myst
RUN pip install git+https://github.com/carlsborg/rcviz.git
RUN pip install jupyterlab-gitlab		
RUN jupyter labextension disable @jupyterlab/extensionmanager-extension

EXPOSE 8000
